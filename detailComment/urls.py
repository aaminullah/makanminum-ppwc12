from django.urls import path
from . import views

app_name = "detailComment"

urlpatterns = [
    path('<str:namaResep>/', views.detail, name='detail'),
]
