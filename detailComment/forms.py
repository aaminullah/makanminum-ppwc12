from django import forms
from .models import Comment


class CommentForm(forms.ModelForm):
    class Meta():
        model = Comment
        fields = ['komentar']
        widgets = {
            'komentar': forms.Textarea(attrs={'rows': 8, 'cols': 40}),
        }
        labels = {
            'komentar': "Berikan komentar untuk resep ini:",
        }
