from django.http import response, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.contrib import messages
from .models import Comment
from tambahupdate.models import Resep
from kategori.models import Kategori
from .forms import CommentForm

# Create your views here.

def detail(request, namaResep):
    resep = Resep.objects.get(nama=namaResep)
    form = CommentForm(request.POST or None)
    if form.is_valid():
        komen = request.POST['komentar']
        resep.comment_set.create(komentar=komen)
        messages.success(request, 'Komentar berhasil ditambahkan!')
        return redirect(f'/detail/{resep.nama}/')
    response = {
        "form": form,
        "resep": resep,
    }
    return render(request, 'detail.html', response)
