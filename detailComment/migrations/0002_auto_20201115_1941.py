# Generated by Django 3.1.1 on 2020-11-15 12:41

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('detailComment', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='comment',
            old_name='desc',
            new_name='komentar',
        ),
    ]
