from django.shortcuts import render,get_object_or_404
from django.http import HttpResponse
from tambahupdate.models import Resep
from .models import Kategori


# Create your views here.
def kategori(request):
    context = {
        'list_resep' : Resep.objects.all().order_by("-id") [:6],
        'list_kategori' : Kategori.objects.all(),
    }
    return render(request, "kategori.html", context)

def spesifik(request, nama):
    context = {
        'kategori' : nama,
        'list_resep_by_kategori' : Resep.objects.filter(kategori = Kategori.objects.get(nama = nama)).order_by("-id")
    }
    return render(request, "kategori_spesifik.html", context)
