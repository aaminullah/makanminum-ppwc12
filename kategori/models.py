from django.db import models
from django.shortcuts import reverse

# Create your models here.
class Kategori(models.Model):
    nama = models.CharField(max_length=25)

    def __str__(self):
        return self.nama

    def get_absolute_url(self):
        return reverse('kategori:kategori_spesifik', args=[self.nama])