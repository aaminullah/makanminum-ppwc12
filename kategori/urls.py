from django.urls import path
from . import views

app_name = 'kategori'

urlpatterns = [
    path('', views.kategori, name="kategori"),
    path('<str:nama>/', views.spesifik, name="kategori_spesifik"),
    ]