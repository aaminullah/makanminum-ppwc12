from django.test import TestCase, Client
from django.apps import apps
from .apps import KategoriConfig
from tambahupdate.models import Resep
from .models import Kategori

# Create your tests here.
class Test_kategori(TestCase):

    def test_is_app_exist(self):
        self.assertEqual(KategoriConfig.name, 'kategori')
        self.assertEqual(apps.get_app_config('kategori').name, 'kategori')

    def test_is_kategori_page_exist(self):
        response = Client().get('/kategori/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "kategori.html")

    def test_is_get_absolute_url_active(self):
        Kategori.objects.create(nama='Daging')
        dummy = Kategori.objects.get(id=1)
        self.assertEqual(dummy.get_absolute_url(), '/kategori/Daging/')

    def test_is_kategori_spesifik_page_exist(self):
        Kategori.objects.create(nama='Daging')
        dummy = Kategori.objects.get(id=1)
        response = Client().get(dummy.get_absolute_url())
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "kategori_spesifik.html")
