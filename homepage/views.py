from django.shortcuts import render
from django.http import HttpResponse
from tambahupdate.models import Resep
import random


def home(request):
    resep = list(Resep.objects.all())
    if len(resep) > 2:
        random_items = random.sample(resep, 3)
        return render(request, 'homepage/home.html', {'list_resep': random_items})
    elif len(resep) > 0:
        return render(request, 'homepage/home.html', {'list_resep': Resep.objects.all()})
    return render(request, 'homepage/home.html')
