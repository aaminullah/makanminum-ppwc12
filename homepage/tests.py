from django.test import TestCase, Client
from django.apps import apps
from django.urls import resolve
from .apps import HomepageConfig

# Create your tests here.
class TestApp(TestCase):
    def test_is_app_available(self):
        self.assertEqual(HomepageConfig.name, 'homepage')
        self.assertEqual(apps.get_app_config('homepage').name, 'homepage')

    def test_is_profile_page_exists(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'homepage/home.html')
