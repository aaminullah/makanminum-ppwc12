from django import forms
from .models import Resep

class ResepForm(forms.ModelForm):
    class Meta():
        model = Resep
        fields = "__all__"
        widgets = {
            'nama' : forms.TextInput(attrs={'placeholder' : 'Rendang'}),
            # 'kategori' : forms.TextInput(attrs={'placeholder' : 'Daging'}),
            'durasi' : forms.TextInput(attrs={'placeholder' : '2 jam'}),
            'porsi' : forms.TextInput(attrs={'placeholder' : '10 Porsi'}),
            'deskripsi' : forms.Textarea(attrs={'placeholder' : 'Maknyos'}),
            'bahan' : forms.Textarea(attrs={'placeholder' : '- Daging sapi\n- Bumbu rendang'}),
            'langkah' : forms.Textarea(attrs={'placeholder' : '- Masak Daging sapinya sampai matang\n- Sajikan'}),
        }
        labels = {
            'nama' : "Nama Menu",
            'kategori' : "Kategori",
            'durasi' : "Durasi",
            'porsi' : "Porsi Saji",
            'deskripsi' : "Deskripsi",
            'bahan' : "Bahan-bahan",
            'langkah' : "Langkah Pembuatan",
            'foto' : "Foto Menu",
        }
