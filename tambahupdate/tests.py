from django.apps import apps
from django.test import TestCase, Client
from .apps import TambahupdateConfig
from .models import Resep
from kategori.models import Kategori

# Create your tests here.
class TestApp(TestCase):
    def setUp(self):
        Kategori.objects.create(nama="Daging")
        dummy = Kategori.objects.get(nama='Daging')
        Resep.objects.create(
            nama = "Rendang",
            kategori = dummy,
            durasi = "2 jam",
            porsi = "10 porsi",
            deskripsi = "Maknyos",
            bahan = "- Daging sapi\n- Bumbu rendang",
            langkah = "- Masak Daging sapinya sampai matang\n- Sajikan"
        )

    def test_is_app_exist(self):
        self.assertEqual(TambahupdateConfig.name, 'tambahupdate')
        self.assertEqual(apps.get_app_config('tambahupdate').name, 'tambahupdate')
    
    def test_is_resep_model_created(self):
        count = Resep.objects.all().count()
        resep = Resep.objects.create(
            nama = "Indomie",
            kategori = Kategori.objects.create(nama='Lain - Lain'),
            durasi = "10 menit",
            porsi = "1 porsi",
            deskripsi = "Seleraku",
            bahan = "1 bungkus indomie",
            langkah = "- Buka bungkus indomienya\n- Rebus mienya sampai matang\n- Sajikan",
        )
        self.assertTrue(isinstance(resep, Resep))
        self.assertEqual(Resep.objects.all().count(), count + 1)

    def test_tulisresep_url_is_exist(self):
        response = Client().get('/resep_form/tulis/')
        self.assertEqual(response.status_code, 200)

    def test_does_create_form_from_page_succeed(self):
        count = Resep.objects.all().count()
        Kategori.objects.create(nama="Lain - Lain")
        dummy = Kategori.objects.get(pk=1).pk
        data = {
            'nama' : 'Indomie',
            'kategori' : dummy,
            'durasi' : '5 menit',
            'porsi' : '2 porsi',
            'deskripsi' : 'Seleraku',
            'bahan' : '1 bungkus indomie dan telur',
            'langkah' : '- Buka bungkusnya\n- masukkan mienya\n- sajikan',
        }
        response = self.client.post('/resep_form/tulis/', data)
        self.assertEqual(Resep.objects.all().count(), count + 1)
        self.assertEqual(response.status_code, 302)
    
    def test_perbaruiresep_url_is_exist(self):
        response = self.client.get(f'/resep_form/perbarui/{Resep.objects.all()[0].id}/')
        self.assertEqual(response.status_code, 200)
    
    def test_does_update_form_from_page_succeed(self):
        resep = Resep.objects.get(nama="Rendang")
        resep.nama = "Gule"
        data = {
            'nama' : 'Gule',
            'kategori' : resep.kategori,
            'durasi' : resep.durasi,
            'porsi' : resep.porsi,
            'deskripsi' : resep.deskripsi,
            'bahan' : resep.bahan,
            'langkah' : resep.langkah,
        }
        response = self.client.post(f'/resep_form/perbarui/{Resep.objects.all()[0].id}/', data)
        Resep.objects.filter(id=1).update(nama="Gule")
        amount = Resep.objects.filter(nama='Rendang').count()
        self.assertEqual(amount, 0)
        self.assertEqual(response.status_code, 200)
