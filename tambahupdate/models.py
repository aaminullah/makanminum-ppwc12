from django.db import models
from kategori.models import Kategori

# Create your models here.
class Resep(models.Model):
    nama = models.CharField(max_length = 150)
    kategori = models.ForeignKey(Kategori, on_delete=models.CASCADE)
    durasi = models.CharField(max_length = 50)
    porsi = models.CharField(max_length = 50)
    deskripsi = models.TextField()
    bahan = models.TextField()
    langkah = models.TextField()
    foto = models.ImageField(null=True, blank=True)