from django.shortcuts import render, redirect
from django.contrib import messages
from .forms import ResepForm
from .models import Resep

def tulis(request):
    if request.method == "POST":
        form = ResepForm(request.POST or None, request.FILES or None)
        
        if form.is_valid():
            nama = form.cleaned_data['nama']
            messages.success(request, f'Resep "{nama}" berhasil ditambahkan!')
            form.save()
            return redirect('/kategori/')
    form = ResepForm()
    context = {
        'form' : form,
        'h1' : 'Tulis Resep Baru',
        'p' : 'Tunjukkan kreativitas Anda dengan membagikan resep karya Anda sendiri.',
    }
    return render(request, "resep_form.html", context)

def perbarui(request, pk):
    resep = Resep.objects.get(id=pk) 
    nama = resep.nama   
    if request.method == "POST":
        form = ResepForm(request.POST or None, request.FILES or None, instance=resep)
        if form.is_valid():
            messages.success(request, f'Resep "{nama}" berhasil diperbarui!')
            form.save()
            return redirect(f'/detail/{resep.nama}/')
    form = ResepForm(instance=resep)
    context = {
        'form' : form,
        'h1' : 'Perbarui Resep',
        'p' : 'Sempurnakan resep yang sudah ada dengan memperbarui informasinya',
    }
    return render(request, "resep_form.html", context)