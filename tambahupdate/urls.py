from django.urls import path
from . import views

app_name = 'tambahupdate'

urlpatterns = [
    path('tulis/', views.tulis, name="tulis"),
    path('perbarui/<int:pk>/', views.perbarui, name="perbarui"),
]