Kelompok PPW C12 - SA
1. Ahmad Aminullah Alfiyanto - 1906399064
2. Aryo Wicaksono - 1906399032
3. Billy Vande Yohannes - 1906398276
4. Lazuardi Pratama Putra Nusantara - 1906398225
5. Raihan Rizqi Muhtadiin - 1906353776

Link Herokuapp : http://makanminum.herokuapp.com/

Pandemi COVID-19 menyebabkan kita harus social distancing dan tidak keluar rumah jika bisa. Kebiasaan dan rutinitas yang biasanya dilakukan terpaksa berubah karena kondisi ini. Para pelajar dari tingkat SD sampai perguruan tinggi pun juga terpaksa untuk belajar online dari rumah. Tidak hanya pelajar, beberapa pekerja kantoran juga harus bekerja dari rumah. Akibatnya, kondisi rumah yang biasanya kosong pada hari kerja menjadi penuh seperti pada hari sabtu dan minggu. Para pelajar dan pekerja kantoran yang biasanya membeli makanan di kantin sekolah atau kantor, sekarang harus membuat makanannya sendiri di rumah. Bukan hanya pelajar dan pekerja kantoran, ibu rumah tangga juga harus memasak tiga kali sehari untuk keluarganya, belum lagi jika ingin membuat makanan-makanan ringan sebagai selingan. Tentunya mereka akan memilih untuk membuat menu makanan yang mudah, murah, praktis, namun bervariasi untuk dibuat. Maka dari itu, kelompok kami memutuskan untuk membuat website yang memuat menu dan resep-resep makanan serta minuman yang relatif mudah untuk dibuat. 

Fitur-fitur aplikasi:
- Home page yang menampilkan rekomendasi resep-resep makanan dan minuman
- Halaman untuk melihat berbagai kategori makanan dan minuman serta resep terbaru
- Halaman untuk melihat detail resep beserta kolom komentar tentang resep tersebut
- Halaman untuk menambahkan resep baru
- Pilihan untuk mengupdate resep yang sudah ada
- Halaman about, yang berisi mengenai latar belakang dan orang-orang dibalik pembangunan website
- Halaman untuk menampung kritik dan saran dari pengunjung untuk pengembang website

[![pipeline status](https://gitlab.com/aaminullah/makanminum-ppwc12/badges/master/pipeline.svg)](https://gitlab.com/aaminullah/makanminum-ppwc12/-/commits/master)
[![coverage report](https://gitlab.com/aaminullah/makanminum-ppwc12/badges/master/coverage.svg)](https://gitlab.com/aaminullah/makanminum-ppwc12/-/commits/master)