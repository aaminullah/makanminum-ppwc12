from django.urls import path
from . import views

app_name = 'kritiksaran'

urlpatterns = [
    path('kritik-dan-saran/', views.kritik_submission_view, name="kritik_submission_view"),
    path('about-us/', views.about_us_view, name="about_us_view"),
    ]