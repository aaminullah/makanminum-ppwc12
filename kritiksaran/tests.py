from django.apps import apps
from django.test import TestCase, Client
from django.urls import resolve
from .apps import KritiksaranConfig
from .models import Kritik
from .views import kritik_submission_view, about_us_view
from . import models

# Create your tests here.
class TestApp(TestCase):
    def test_is_app_available(self):
        self.assertEqual(KritiksaranConfig.name, 'kritiksaran')
        self.assertEqual(apps.get_app_config('kritiksaran').name, 'kritiksaran')
    
    def test_is_kritik_model_created(self):
        kritikan = Kritik.objects.create(isi="Wah websitenya keren! Semangat terus buat developernya!")
        self.assertTrue(isinstance(kritikan, Kritik))
        self.assertEqual(len(Kritik.objects.all()), 1)
    
    def test_is_kritik_submission_page_exists(self):
        response = Client().get('/misc/kritik-dan-saran/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "kritik-submission.html")
        found = resolve('/misc/kritik-dan-saran/')
        self.assertEqual(found.func, kritik_submission_view)

    def test_is_form_submission_succeed(self):
        response = Client().post('/misc/kritik-dan-saran/', data={'isi': "Sangat senang dengan keberadaan MakanMinum."})
        amount = Kritik.objects.all().count()
        self.assertEqual(amount, 1)

    def test_is_about_us_page_exists(self):
        response = Client().get('/misc/about-us/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "about-us.html")
        found = resolve('/misc/about-us/')
        self.assertEqual(found.func, about_us_view)