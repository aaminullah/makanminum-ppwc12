from django import forms
from .models import Kritik
from django.forms import ModelForm, Textarea

class KritikForm(ModelForm):
    class Meta:
        model = Kritik
        fields = "__all__"
        widgets = {"isi": Textarea(attrs={'rows': 8, 'cols': 40, 'required': True})}