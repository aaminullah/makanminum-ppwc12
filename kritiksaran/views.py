from django.shortcuts import render, redirect
from django.contrib import messages
from django.http import HttpResponseRedirect
from .models import Kritik
from .forms import KritikForm

# Create your views here.
def kritik_submission_view(request):
    my_form = KritikForm(request.POST or None)
    if my_form.is_valid():
        Kritik.objects.create(**my_form.cleaned_data)
        messages.success(request, 'Terimakasih untuk masukannya!')
        return HttpResponseRedirect('')
    else:
        messages.error(request, 'Mohon maaf, ada kesalahan. Harap coba lagi.')
    return render(request, "kritik-submission.html", {"form": my_form})

def about_us_view(request):
    return render(request, "about-us.html")